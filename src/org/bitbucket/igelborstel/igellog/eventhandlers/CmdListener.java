package org.bitbucket.igelborstel.igellog.eventhandlers;

import org.bitbucket.igelborstel.igellog.main.IgelLog;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CmdListener implements Listener
{
	IgelLog mainPlugin;
	
	public CmdListener(IgelLog m)
	{
		this.mainPlugin = m;
	}
	
	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent event)
	{
		if(event.getPlayer().hasPermission("igellog.ignorefilter.cmd"))
		{
			return;
		}
	}
}
