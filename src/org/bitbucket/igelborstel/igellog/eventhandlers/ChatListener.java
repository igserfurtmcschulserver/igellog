package org.bitbucket.igelborstel.igellog.eventhandlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bitbucket.igelborstel.igellog.main.IgelLog;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener
{
	IgelLog mainPlugin;
	
	public ChatListener(IgelLog m)
	{
		this.mainPlugin = m;
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event)
	{
		if(event.getPlayer().hasPermission("igellog.ignorefilter")) //save time
		{
			return;
		}
		if(containsBlacklistedWord(event.getMessage()))
		{
			this.mainPlugin.returnDatabase().doUpdate("INSERT INTO `" + this.mainPlugin.getTablePrefix() + "chatmsg`(`text`, `player`, `playeruuid`) VALUES (\'" + event.getMessage() + "\',\'" + event.getPlayer().getName() + "\',\'" + event.getPlayer().getUniqueId() + "\');");
		}
	}
	
	private boolean containsBlacklistedWord(String message)
	{
		ResultSet results = this.mainPlugin.returnDatabase().doQuery("SELECT word FROM `" + this.mainPlugin.getTablePrefix() + "blacklist`");
		ArrayList<String> blacklistedWords = new ArrayList<String>();
		try 
		{
			while(results.next())
			{
				String word = results.getString("word");
				blacklistedWords.add(word);
			}
		} 
		catch(SQLException e) 
		{
			System.err.println("Fehler bei der Auswertung des Resultsets! Fehler: ");
			e.printStackTrace();
			return false;
		}
		String msg = message;
		msg.replaceAll("\\s+","");
		for(String blacklistedWord : blacklistedWords)
		{
			if(msg.toLowerCase().contains(blacklistedWord.toLowerCase()))
			{
				return true;
			}
		}
		return false;
	}
}
