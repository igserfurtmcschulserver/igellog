package org.bitbucket.igelborstel.igellog.eventhandlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.bitbucket.igelborstel.igellog.main.IgelLog;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


public class JoinListener implements Listener
{
	private IgelLog mainPlugin;
	public JoinListener(IgelLog m)
	{
		this.mainPlugin = m;
	}
	
	@EventHandler
	public void onPlayerLogin(PlayerJoinEvent playerlogin)
	{
		if(playerlogin.getPlayer().hasPermission("igellog.viewnews"))
		{
			Timestamp logouttime = null;
			ResultSet results = this.mainPlugin.returnDatabase().doQuery("SELECT `time` FROM `" + this.mainPlugin.getTablePrefix() + "logouttime` WHERE `playeruuid`=\'" + playerlogin.getPlayer().getUniqueId() + "\'");
			try 
			{
				while(results.next())
				{
					logouttime = results.getTimestamp("time");
				}
			} 
			catch (SQLException e) 
			{
				System.err.println("[IgelLog] Error selecting time from database for player");
				e.printStackTrace();
			}
			
			int newMessageCounter = 0;
			if(logouttime != null)
			{
				results = this.mainPlugin.returnDatabase().doQuery("SELECT `id` FROM `" + this.mainPlugin.getTablePrefix() + "chatmsg` WHERE `time` >= \'" + logouttime + "\'");
				try 
				{
					while(results.next())
					{
						++newMessageCounter;
					}
				} 
				catch (SQLException e) 
				{
					System.err.println("[IgelLog] Error checking for new messages");
					e.printStackTrace();
				}
			}
			playerlogin.getPlayer().sendMessage("[Igellog] Es wurde in deiner Abwesenheit " + newMessageCounter + " Nachrichten mit bedenklichen W�ertern geschrieben!");
			playerlogin.getPlayer().sendMessage("[Igellog] Rufe /msglog list auf um die Nachrichten zu lesen!");
		}
		if(playerlogin.getPlayer().hasPermission("igellog.viewnews.cmd"))
		{
			playerlogin.getPlayer().sendMessage("[Igellog] Admin-Neuigkeiten");
		}
	}

}
