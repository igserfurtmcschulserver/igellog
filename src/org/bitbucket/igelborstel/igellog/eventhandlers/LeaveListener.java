package org.bitbucket.igelborstel.igellog.eventhandlers;

import org.bitbucket.igelborstel.igellog.main.IgelLog;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveListener implements Listener 
{
	private IgelLog mainPlugin;
	
	public LeaveListener(IgelLog m)
	{
		this.mainPlugin = m;
	}

	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event)
	{
		if(event.getPlayer().hasPermission("igellog.viewnews") || event.getPlayer().hasPermission("igellog.viewnews.cmd"))
		{
		    this.mainPlugin.returnDatabase().doUpdate("INSERT ON DUPLICATE KEY UPDATE`" + this.mainPlugin.getTablePrefix() + "logouttime` (`playeruuid`) VALUES (\'" + event.getPlayer().getUniqueId() + "\') ON DUPLICATE KEY UPDATE `time`=NOW()");
			//this.mainPlugin.returnDatabase().doUpdate("UPDATE `" + this.mainPlugin.getTablePrefix() + "logouttime` SET time=NOW() WHERE `playeruuid`=\'" + event.getPlayer().getUniqueId() + "\')");
		}
	}
}
