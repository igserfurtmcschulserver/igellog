package org.bitbucket.igelborstel.igellog.commandhandlers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bitbucket.igelborstel.igellog.main.IgelLog;
import org.bitbucket.igelborstel.igellog.main.LogMessage;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MsgLogCommands implements CommandExecutor 
{

	private IgelLog mainPlugin;
	private ArrayList<LogMessage> logMessages;
	
	public MsgLogCommands(IgelLog m) 
	{
		this.mainPlugin = m;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if(cmd.getName().equalsIgnoreCase("msglog"))
		{
			if(args.length > 1 && args.length < 2)
			{
				if(args[0].equalsIgnoreCase("list"))
				{
					if(sender.hasPermission("igellog.msglog.list"))
					{
						sender.sendMessage("[IgelLog] Folgende Nachrichten wurden geschrieben");
						sender.sendMessage("[IgelLog] Format: id) [Zeit] Spieler > Nachricht");
						if(args.length == 1)
						{
							loadLogMessages(5);
							for(LogMessage msg : logMessages)
							{
								sender.sendMessage(msg.getId() +  ") [" + msg.getTime() + "] " + msg.getPlayer() + " > " + msg.getText());
							}
							sender.sendMessage("Um mehr zu sehen benutze /msglog list <Seite>");
						}
						if(args.length == 2)
						{
							int seite;
							try
							{
								seite = Integer.parseInt(args[1]);
							}
							catch(NumberFormatException e)
							{
								sender.sendMessage("Das zweite Argument muss eine Zahl sein!");
								return false;
							}
							loadLogMessagesFull();
							if(seite*5 > logMessages.size())
							{
								sender.sendMessage("Soviele Seiten gibt es garnicht");
								return true;
							}
							else
							{
								sender.sendMessage("[IgelLog] Folgende Nachrichten wurden geschrieben");
								sender.sendMessage("[IgelLog] Format: id) [Zeit] Spieler > Nachricht");
								int i = 0, iterations = 0;
								i = seite*5 - 5;
								while(iterations < 5)
								{
									sender.sendMessage(logMessages.get(i).getId() +  ") [" + logMessages.get(i).getTime() + "] " + logMessages.get(i).getPlayer() + " > " + logMessages.get(i).getText());
									++i;
									++iterations;
								}
							}
						}
						return true;
					}
					else
						return true;
				}
				else if(args[0].equalsIgnoreCase("getuuid"))
				{
					if(sender.hasPermission("igellog.msglog.viewuuid"))
					{
						if(args.length > 2)
						{
							return false;
						}
						else
						{
							int msgid;
							try
							{
								msgid = Integer.parseInt(args[1]);
							}
							catch(NumberFormatException e)
							{
								sender.sendMessage("Das zweite Argument muss eine Zahl sein!");
								return false;
							}
							ResultSet results = this.mainPlugin.returnDatabase().doQuery("SELECT playeruuid FROM `" + this.mainPlugin.getTablePrefix() + "_chatmsg` WHERE id=\'" + msgid + "\'");
							String uuid = "Error";
							try 
							{
								while(results.next())
								{
									uuid = results.getString("playeruuid");
								}
							} 
							catch (SQLException e) 
							{
								System.err.println("[IgelLog] Fehler beim Abfragen der Meldungen aus der Datenbank");
								e.printStackTrace();
							}
							sender.sendMessage("Die zur Nachricht " + msgid + " dazugehörige UUID ist " + uuid);
							
						}
					}
					return true;
				}
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}
	
	private void loadLogMessagesFull()
	{
		ResultSet results = this.mainPlugin.returnDatabase().doQuery("SELECT * FROM `" + this.mainPlugin.getTablePrefix() + "_chatmsg` ORDER BY time DESC");
		try 
		{
			while(results.next())
			{
				LogMessage temp = new LogMessage();
				temp.setId(results.getInt("id"));
				temp.setPlayer(results.getString("player"));
				temp.setText(results.getString("text"));
				temp.setTime(results.getString("time"));
				logMessages.add(temp);
				
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("[IgelLog] Fehler beim Abfragen der Meldungen aus der Datenbank");
			e.printStackTrace();
		}
	}
	
	private void loadLogMessages(int limit)
	{
		ResultSet results = this.mainPlugin.returnDatabase().doQuery("SELECT * FROM `" + this.mainPlugin.getTablePrefix() + "_chatmsg` ORDER BY time DESC LIMIT 0," + limit);
		try 
		{
			while(results.next())
			{
				LogMessage temp = new LogMessage();
				temp.setId(results.getInt("id"));
				temp.setPlayer(results.getString("player"));
				temp.setText(results.getString("text"));
				temp.setTime(results.getString("time"));
				logMessages.add(temp);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("[IgelLog] Fehler beim Abfragen der Meldungen aus der Datenbank");
			e.printStackTrace();
		}
	}

}
