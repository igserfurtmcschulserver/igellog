package org.bitbucket.igelborstel.igellog.commandhandlers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bitbucket.igelborstel.igellog.main.IgelLog;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BlacklistCommands implements CommandExecutor 
{
	IgelLog mainPlugin;

	public BlacklistCommands(IgelLog m)
	{
		this.mainPlugin = m;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		if(command.getName().equalsIgnoreCase("filterblacklist"))
		{
			if(args.length < 1)
			{
				return false;
			}
			else if(args.length > 2)
			{
				return false;
			}
			
			if(args[0].equalsIgnoreCase("list") && (sender.hasPermission("igellog.blacklist.list") || sender.isOp()))
			{
				sender.sendMessage("[IgelLog] Lade Blacklist...");
				ResultSet results = this.mainPlugin.returnDatabase().doQuery("SELECT * FROM `" + this.mainPlugin.getTablePrefix() + "blacklist`");
				sender.sendMessage("Format: [ID] [Wort]");
				try 
				{
					while(results.next())
					{
						int id = results.getInt("id");
						String word = results.getString("word");
						sender.sendMessage("["+ id + "] " + word);
					}
					return true;
				} 
				catch(SQLException e) 
				{
					sender.sendMessage("Fehler bei der Abfrage der Blacklist");
					System.err.println("Fehler bei der Auswertung des Resultsets! Fehler: ");
					e.printStackTrace();
					return true;
				}
				
			}
			else if(args[0].equalsIgnoreCase("list") && !(sender.hasPermission("igellog.blacklist.list") || sender.isOp()))
			{
				return true;
			}
			else if(args[0].equalsIgnoreCase("remove") && (sender.hasPermission("igellog.blacklist.remove") || sender.isOp()))
			{
				if(args.length > 1)
				{
					int id;
					try
					{
						id = Integer.parseInt(args[1]);
					}
					catch(NumberFormatException e)
					{
						sender.sendMessage("Das zweite Argument muss eine Zahl sein!");
						return false;
					}
					sender.sendMessage("L�sche das Wort mit der ID " + id + "aus der Datenbank");
					this.mainPlugin.returnDatabase().doUpdate("DELETE FROM `" + this.mainPlugin.getTablePrefix() + "blacklist` WHERE id=" + id);
					sender.sendMessage("Wort erfolgreich gel�scht!");
					return true;
				}
			}
			else if(args[0].equalsIgnoreCase("remove") && !(sender.hasPermission("igellog.blacklist.remove") || sender.isOp()))
			{
				return true;
			}
			else if(args[0].equalsIgnoreCase("add") && (sender.hasPermission("igellog.blacklist.add") || sender.isOp()))
			{
				if(args.length > 1)
				{
					sender.sendMessage("F�ge ein neues Wort zur Blacklist hinzu: " + args[1]);
					this.mainPlugin.returnDatabase().doUpdate("INSERT INTO `" + this.mainPlugin.getTablePrefix() + "blacklist`(`word`) VALUES (\'" + args[1] + "\');");
					return true;
				}
				else
				{
					return false;
				}
				
			}
			else if(args[0].equalsIgnoreCase("add") && !(sender.hasPermission("igellog.blacklist.add") || sender.isOp()))
			{
				return true;
			}
			else if(args[0].equalsIgnoreCase("reload") && (sender.hasPermission("igellog.blacklist.reload") || sender.isOp()))
			{
				sender.sendMessage("currently not implemented and not necessary");
				return true;
			}
			else if(args[0].equalsIgnoreCase("reload") && !(sender.hasPermission("igellog.blacklist.reload") || sender.isOp()))
			{
				
			}
			else
			{
				return false;
			}
		}
		return false;
	}
	

}
