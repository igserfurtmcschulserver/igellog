package org.bitbucket.igelborstel.igellog.main;

import org.bitbucket.igelborstel.igelcore.database.IDatabase;
import org.bitbucket.igelborstel.igelcore.database.MySql;
import org.bitbucket.igelborstel.igelcore.database.SqLite;
import org.bitbucket.igelborstel.igellog.commandhandlers.BlacklistCommands;
import org.bitbucket.igelborstel.igellog.commandhandlers.MsgLogCommands;
import org.bitbucket.igelborstel.igellog.eventhandlers.ChatListener;
import org.bitbucket.igelborstel.igellog.eventhandlers.CmdListener;
import org.bitbucket.igelborstel.igellog.eventhandlers.JoinListener;
import org.bitbucket.igelborstel.igellog.eventhandlers.LeaveListener;
import org.bukkit.plugin.java.JavaPlugin;

public class IgelLog extends JavaPlugin 
{
	private IDatabase database;
	private boolean useMySql = false;
	private String tableprefix = "igellog_";
	
	@Override
	public void onEnable()
	{
		if(!this.getServer().getPluginManager().isPluginEnabled(this.getDescription().getDepend().get(0)))
		{
			System.out.println("[IgelLog] IgelCore not found. Disabling IgelLog");
			System.out.println("[IgelLog] Contact your Pluginprovider to get additional Information");
			this.getServer().getPluginManager().disablePlugin(this);
		}
		
		loadConfig();
		
		if(useMySql)
		{
			this.database = new MySql(this.getDescription().getName());
		}
		else
		{
			this.database = new SqLite(this.getDescription().getName());
		}
		
		createTables();
		
		this.getCommand("filterblacklist").setExecutor(new BlacklistCommands(this));
		this.getCommand("msglog").setExecutor(new MsgLogCommands(this));
		
		
		this.getServer().getPluginManager().registerEvents(new JoinListener(this), this);
		this.getServer().getPluginManager().registerEvents(new ChatListener(this), this);
		this.getServer().getPluginManager().registerEvents(new CmdListener(this), this);
		this.getServer().getPluginManager().registerEvents(new LeaveListener(this), this);
	}

	
	private void createTables() 
	{
		if(this.database instanceof SqLite)
		{
			if(this.database.connect())
			{
				this.database.doUpdate("CREATE TABLE IF NOT EXISTS `" + tableprefix + "blacklist` (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, word VARCHAR(50))");
				this.database.doUpdate("CREATE TABLE IF NOT EXISTS `" + this.tableprefix + "chatmsg` (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, text VARCHAR(160), player VARCHAR(16), playeruuid VARCHAR(36), time DATETIME DEFAULT CURRENT_TIMESTAMP)");
				this.database.doUpdate("CREATE TABLE IF NOT EXISTS `" + this.tableprefix + "logouttime` (id INTEGER NOT NULL AUTOINCREMENT, playeruuid VARCHAR(36) PRIMARY KEY, time DATETIME DEFAULT CURRENT_TIMESTAMP)");
			}
		}
		else if(this.database instanceof MySql)
		{
			if(this.database.connect())
			{
				this.database.doUpdate("CREATE TABLE IF NOT EXISTS `" + tableprefix + "blacklist` (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, word VARCHAR(50))");
				this.database.doUpdate("CREATE TABLE IF NOT EXISTS `" + this.tableprefix + "chatmsg` (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, text VARCHAR(160), player VARCHAR(16), playeruuid VARCHAR(36), time TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
				this.database.doUpdate("CREATE TABLE IF NOT EXISTS `" + this.tableprefix + "logouttime` (id INT NOT NULL AUTO_INCREMENT, playeruuid VARCHAR(36) PRIMARY KEY, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
			}
		}
	}

	public IDatabase returnDatabase()
	{
		return this.database;
	}
	
	public String getTablePrefix()
	{
		return this.tableprefix;
	}
	
	private void loadConfig()
	{
		this.reloadConfig();
		this.getConfig().options().header("Igellog Configuration");
		this.getConfig().addDefault("igellog.database.usemysql", false);
		this.getConfig().addDefault("igellog.database.tableprefix", "igellog_");
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		
		this.useMySql = this.getConfig().getBoolean("igellog.database.usemysql");
		this.tableprefix = this.getConfig().getString("igellog.database.tableprefix");
	}
	
	
	
	@Override
	public void onDisable()
	{
		if(this.database.isConnected())
		{
			this.database.close();
		}
	}
}
