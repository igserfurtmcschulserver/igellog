package org.bitbucket.igelborstel.igellog.main;

public class LogMessage 
{
	private String text;
	private String player;
	private String playeruuid;
	private String time;
	private int id;
	
	
	public String getText() 
	{
		return text;
	}
	public void setText(String text) 
	{
		this.text = text;
	}
	
	public String getPlayer() 
	{
		return player;
	}
	public void setPlayer(String player) 
	{
		this.player = player;
	}
	
	public String getTime() 
	{
		return time;
	}
	public void setTime(String time) 
	{
		this.time = time;
	}
	
	public int getId() 
	{
		return id;
	}
	public void setId(int id) 
	{
		this.id = id;
	}
	
	public String getPlayeruuid() 
	{
		return playeruuid;
	}
	
	public void setPlayeruuid(String playeruuid) 
	{
		this.playeruuid = playeruuid;
	}
	
	

}
